SpriteFrameCmp = class("SpriteFrameCmp")

function SpriteFrameCmp:__init(anim, animDt, maxFrame)
   self.anim = anim
   self.curDt = 0
   self.animDt = animDt
   self.curFrame = 1
   self.maxFrame = maxFrame
end

function SpriteFrameCmp:refresh(anim, animDt, maxFrame)
   self.anim = anim
   self.curDt = 0
   self.animDt = animDt
   self.curFrame = 1
   self.maxFrame = maxFrame
end

function SpriteFrameCmp:copy()
   return SpriteFrameCmp(self.anim, self.animDt, self.maxFrame)
end
