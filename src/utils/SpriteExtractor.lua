SpriteExtractor = class("SpriteExtractor")

function SpriteExtractor:pullAnimation(img, tileWidth, tileHeight,
                                       frameStart, frameEnd)
   local quads = {}
   local imgWidth = img:getWidth()
   local imgHeight = img:getHeight()

   local maxFrameWidth = math.floor(imgWidth / tileWidth)
   if maxFrameWidth == 0 then
      print("Invalid tileWidth")
   else
      local widthIdx = 0
      local heightIdx = 0
      local xStart = 0
      local yStart = 0
      for i=frameStart - 1, frameEnd - 1, 1 do
         if widthIdx == maxFrameWidth then
            widthIdx = 0
            heightIdx = heightIdx + 1
         end
         xStart = tileWidth * widthIdx
         yStart = tileHeight * heightIdx
         quads[#quads + 1] = love.graphics.newQuad(xStart, yStart,
               tileWidth, tileHeight, imgWidth, imgHeight)
         widthIdx = widthIdx + 1
      end
   end

   return quads;
end
