-- Core Libraries
require("src/core/resources")
require("src/core/state")

-- Systems
require("src/systems/renderers/RenderTextSys")
require("src/systems/events/MenuSys")

-- Components
require("src/components/physics/PositionCmp")
require("src/components/renderers/AlignCmp")
require("src/components/renderers/TextCmp")

-- Events
require("src/events/inputs/keyPressed")

-- Helpers
require("src/creators/MenuCreator")

PauseState = class("PauseState", States)

function PauseState:__init(screenShot)
   self.screenShot = screenShot
end

function PauseState:load()
   -- Lovetoys
   self.engine = Engine()
   self.eventManager = EventManager()

   -- Event System
   self.menuSys = MenuSys({ 255, 128, 0, 255 }, { 255, 255, 255, 255})

   -- Event Handling
   self.eventManager:addListener("KeyPressed", { self.menuSys, self.menuSys.fireEvent })

   -- Draw Systems
   self.engine:addSystem(RenderTextSys(), "draw", 11)

   self:createOptions()
end

function PauseState:update(dt)
   self.engine:update(dt)
end

function PauseState:draw()
   if self.screenShot then
      love.graphics.setColor(255, 255, 255, 128)
      love.graphics.draw(self.screenShot)
   end
   self.engine:draw()

   love.graphics.setColor(255, 255, 255, 255)
end

function PauseState:keypressed(key, isRepeat)
   self.eventManager:fireEvent(KeyPressed(key, isRepeat))
end

function PauseState:mousepressed(x, y, button)
end

function PauseState:mousereleased(x, y, button)
end

function PauseState:createOptions()
   local posY = love.window.getHeight() / 2
   local posX = love.window.getWidth() / 2 - 100

   local resume = MenuCreator:createMenuItemSel(self.menuSys, resumeFunc, "Resume",
         posX, posY)
   posY = posY + 32 
   local exit = MenuCreator:createMenuItem(self.menuSys, exitFunc, "Exit",
         posX, posY)

   self.engine:addEntity(resume)
   self.engine:addEntity(exit)
end

function exitFunc(cmd)
   if cmd == "enter" then
      love.event.quit()
   elseif (cmd == "left") then
      local stackMenuSys = stack:current().menuSys
      stackMenuSys.curSel = stackMenuSys.curSel - 1
   end
end

function resumeFunc(cmd)
   if cmd == "enter" then
      stack:pop()
   elseif (cmd == "right") then
      local stackMenuSys = stack:current().menuSys
      stackMenuSys.curSel = stackMenuSys.curSel + 1
   end
end
