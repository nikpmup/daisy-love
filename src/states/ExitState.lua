-- Core Libraries
require("src/core/resources")
require("src/core/state")

-- Systems
require("src/systems/renderers/RenderTextSys")
require("src/systems/events/MenuSys")

-- Components
require("src/components/physics/PositionCmp")
require("src/components/renderers/AlignCmp")
require("src/components/renderers/TextCmp")

-- Events
require("src/events/inputs/keyPressed")

-- Helpers
require("src/creators/MenuCreator")

ExitState = class("ExitState", States)

function ExitState:__init(screenShot)
   self.screenShot = screenShot
end

function ExitState:load()
   -- Lovetoys
   self.engine = Engine()
   self.eventManager = EventManager()

   -- Event System
   self.menuSys = MenuSys({ 255, 128, 0, 255 }, { 255, 255, 255, 255})

   -- Event Handling
   self.eventManager:addListener("KeyPressed", { self.menuSys, self.menuSys.fireEvent })

   -- Draw Systems
   self.engine:addSystem(RenderTextSys(), "draw", 11)

   self:createOptions()
end

function ExitState:update(dt)
   self.engine:update(dt)
end

function ExitState:draw()
   if self.screenShot then
      love.graphics.setColor(255, 255, 255, 128)
      love.graphics.draw(self.screenShot)
   end
   self.engine:draw()

   love.graphics.setColor(255, 255, 255, 255)
end

function ExitState:keypressed(key, isRepeat)
   self.eventManager:fireEvent(KeyPressed(key, isRepeat))
end

function ExitState:mousepressed(x, y, button)
end

function ExitState:mousereleased(x, y, button)
end

function ExitState:createOptions()
   local posY = love.window.getHeight() / 2
   local posX = love.window.getWidth() / 2 - 100

   local retry = MenuCreator:createMenuItemSel(self.menuSys, popLoadFunc, "Retry",
         posX, posY)
   posY = posY + 32 
   local menu = MenuCreator:createMenuItem(self.menuSys, menuFunc, "Menu",
         posX, posY)
   posY = posY + 32 
   local exit = MenuCreator:createMenuItem(self.menuSys, exitFunc, "Exit",
         posX, posY)

   self.engine:addEntity(retry)
   self.engine:addEntity(menu)
   self.engine:addEntity(exit)
end

function popLoadFunc(cmd)
   if cmd == "enter" then
      stack:popload()
   end
end

function menuFunc(cmd)
   if cmd == "enter" then
      stack:pop()
      stack:pop()
   end
end
