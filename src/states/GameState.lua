-- Core Libraries
require("src/core/resources")
require("src/core/state")

-- Systems
require("src/systems/animations/SpriteFrameSys")
require("src/systems/animations/SpriteModeSys")
require("src/systems/gameplay/SpawnSys")
require("src/systems/gameplay/EndGameSys")
require("src/systems/gameplay/PlayerPhysSys")
require("src/systems/gameplay/EnemyPhysSys")
require("src/systems/renderers/RenderImageSys")
require("src/systems/physics/PhysicsSys")

-- Components
require("src/components/animations/SpriteFrameCmp")
require("src/components/animations/SpriteModeCmp")
require("src/components/renderers/ImageCmp")
require("src/components/renderers/ScaleCmp")
require("src/components/renderers/QuadCmp")
require("src/components/identity/IsPlayer")
require("src/components/identity/IsEnemy")
require("src/components/physics/ForceCmp")
require("src/components/physics/PositionCmp")
require("src/components/physics/PhysicsCmp")
require("src/components/physics/VelocityCmp")

-- Events
require("src/events/gameplay/EndGameEvent")
require("src/events/physics/BeginContact")

-- States
require("src/states/ExitState")

-- Creators
require("src/creators/GameCreator")
require("src/creators/BubbleCreator")

GameState = class("GameState", State)

function GameState:load()
   -- Load LoveToys
   self.engine = Engine()
   self.eventManager = EventManager()
   self.collManager = CollisionManager()
   self.world = lPhy.newWorld(0, 0, false)
   self.world:setCallbacks(beginContact)

   -- Logic Systems
   self.engine:addSystem(SpriteModeSys(), "logic", 1)
   self.engine:addSystem(SpriteFrameSys(), "logic", 2)
   self.engine:addSystem(SpawnSys(2, 100), "logic", 4)
   self.engine:addSystem(EnemyPhysSys(200, self.engine), "logic", 4)
   self.engine:addSystem(PhysicsSys(), "logic", 5)
   self.engine:addSystem(PlayerPhysSys(0, 10, 0, 100, 
         lWin.getHeight() * 0.9, lWin.getWidth() * 0.2), "logic", 6)

   -- Draw Systems
   self.engine:addSystem(RenderImageSys(), "draw", 11)

   -- Event Handling
   local endGameSys = EndGameSys()
   self.eventManager:addListener("BeginContact", { self.collManager,
         self.collManager.fireEvent } )
   self.eventManager:addListener("EndGameEvent", { endGameSys, endGameSys.fireEvent })

   -- Collision Manager
   self.collManager:addCollisionAction(endGameSys.component1, endGameSys.component2, endGameSys)

   -- Draw Systems

   -- Logic Systems

   -- Create Entities
   self:generatePlayer()
   self.bubbleCreator = BubbleCreator(16, -100, 0.0001)

   -- Variable
   local img = resources.images.background
   self.bgX = 0
   self.bgY = 0
end

function GameState:update(dt)
   self.bubbleCreator:update(dt)
   self.world:update(dt)
   self.engine:update(dt)
end

function GameState:draw()
   lGfx.draw(resources.images.background, self.bgX, self.bgY)
   self.engine:draw()

   -- Resets drawing color
   lGfx.setColor(255, 255, 255, 255)
end

function GameState:keypressed(key, isRepeat)
   --self.eventManager:fireEvent(MousePressed(x, y, button, true))
end

function GameState:mousepressed(x, y, button)
end

function GameState:mousereleased(x, y, button)
end

function GameState:generatePlayer()
   local player = GameCreator:createBoundedImage(resources.images.dandelion,
         lWin.getWidth() * 0.2, lWin.getHeight() / 2, self.world, "dynamic")

   local animation = {
      SpriteFrameCmp(resources.animations.dendelion_float, 0.2, 4)
   }

   player:add(animation[1])
   player:add(SpriteModeCmp("loop", animation))
   player:add(QuadCmp(animation[1].anim[1]))
   

   player:add(IsPlayer())
   self.engine:addEntity(player)
end

function beginContact(fixtureA, fixtureB, contact)
   stack:current().eventManager:fireEvent(BeginContact(fixtureA, fixtureB, contact))
end
