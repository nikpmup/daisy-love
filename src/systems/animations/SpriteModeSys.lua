SpriteModeSys = class("SpriteModeSys", System)

function SpriteModeSys:update(dt)
   for i, entity in pairs(self.targets) do
      local spriteFrame = entity:get("SpriteFrameCmp")
      
      if spriteFrame.curFrame >= spriteFrame.maxFrame then
         local spriteMode = entity:get("SpriteModeCmp")
         if spriteMode.curAnim == #spriteMode.animTbl then
            if spriteMode.mode == "loop" then
               spriteMode.curAnim = 1
               self:copySpriteFrame(spriteMode.animTbl[spriteMode.curAnim],
                     spriteFrame)
            end
         else
            spriteMode.curAnim = spriteMode.curAnim + 1
            self:copySpriteFrame(spriteMode.animTbl[spriteMode.curAnim],
                  spriteFrame)
         end
      end
   end
end

function SpriteModeSys:copySpriteFrame(srcFrame, outFrame)
   outFrame:refresh(srcFrame.anim, srcFrame.animDt, srcFrame.maxFrame)
end

function SpriteModeSys:requires()
   return { "SpriteModeCmp", "SpriteFrameCmp" }
end
