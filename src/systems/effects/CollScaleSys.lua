CollScaleSys = class("CollScaleSys")

function CollScaleSys:__init()
   self.component1 = "ScaleEffectCmp"
   self.component2 = "Everything"
end

function CollScaleSys:action(entities)
   self:scaleEntity(entities.entity1)
   self:scaleEntity(entities.entity2)
end

function CollScaleSys:scaleEntity(entity)
   local effect = entity:get("ScaleEffectCmp")
   local scale = entity:get("ScaleCmp")

   if effect ~= nil and scale ~= nil then
      local randomX = love.math.random(effect.minX, effect.maxX) / 1000
      local randomY = love.math.random(effect.minY, effect.maxY) / 1000
      local time = love.math.random(effect.minTime, effect.maxTime) / 1000

      flux.to(scale, time, { x = randomX, y = randomY })
            :after(scale, time, { x = scale.x, y = scale.y })
   end
end
