RenderImageSys = class("RenderImageSys", System)

function RenderImageSys:draw()
   for i, v in pairs(self.targets) do
      local position = v:get("PositionCmp")
      local image = v:get("ImageCmp")
      local scale = v:get("ScaleCmp")
      -- Checks for physics to grab rotation
      local physics = v:get("PhysicsCmp")
      local angle = physics and physics.fixture:getBody():getAngle() or 0

      -- Checsk for quads
      local quad = v:get("QuadCmp")

      local hWidth = image.img:getWidth() / 2
      local hHeight = image.img:getHeight() / 2
      if quad then
         love.graphics.draw(image.img, quad.quad,
               position.x, position.y, angle, scale.x, scale.y, hHeight, hHeight)
      else
         love.graphics.draw(image.img, position.x, position.y,
               angle, scale.x, scale.y, hWidth, hHeight)
      end

   end
end

function RenderImageSys:requires()
   return { "PositionCmp", "ImageCmp", "ScaleCmp" }
end
