PlayerPhysSys = class("PlayerPhysSys", System)

function PlayerPhysSys:__init(xVMin, yVMin, xVelLimit, yVelLimit, 
      yPosLimit, xClamp)
   self.xVMin = xVMin
   self.yVMin = yVMin
   self.xVelLimit = xVelLimit
   self.yVelLimit = yVelLimit
   self.yPosLimit = yPosLimit
   self.xClamp = xClamp 
end

function PlayerPhysSys:update(dt)
   for i, entity in pairs(self.targets) do
      local pos = entity:get("PositionCmp")
      local phys = entity:get("PhysicsCmp")

      local vel = {
         x, y
      }
      vel.x, vel.y = phys.body:getLinearVelocity()
      self:clampVelocity(vel)
      self:posLimit(pos)
      self:syncPhys(pos, vel, phys.body)
   end
end

function PlayerPhysSys:clampVelocity(vel)
   if vel.x > self.xVelLimit then
      vel.x = self.xVelLimit
   end
   
   if vel.y > self.yVelLimit then
      vel.y = self.yVelLimit
   end

   if vel.x < self.xVMin then
      vel.x = self.xVMin
   end

   if vel.y < self.yVMin then
      vel.y = self.yVMin
   end

end

function PlayerPhysSys:posLimit(pos)
   if pos.y > self.yPosLimit then
      stack:current().eventManager:fireEvent(EndGameEvent())
   end

   if pos.x ~= self.xClamp then
      pos.x = self.xClamp
   end
end

function PlayerPhysSys:syncPhys(pos, vel, phys)
   phys:setPosition(pos.x, pos.y)
   phys:setLinearVelocity(vel.x, vel.y)
end

function PlayerPhysSys:requires()
   return { "PositionCmp", "PhysicsCmp", "IsPlayer" }
end
