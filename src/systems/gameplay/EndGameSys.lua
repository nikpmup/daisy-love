EndGameSys = class("EndGameSys")

function EndGameSys:__init()
   self.component1 = "IsPlayer"
   self.component2 = "IsEnemy"
end

function EndGameSys:action(entities)
   self:endGame()
end

function EndGameSys:fireEvent(event)
   self:endGame()
end

function EndGameSys:endGame()
   local screenData = love.graphics.newScreenshot()
   local screenShot = love.graphics.newImage(screenData)

   stack:push(ExitState(screenShot))
end
