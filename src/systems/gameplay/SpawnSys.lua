SpawnSys = class("SpawnSys", System)

function SpawnSys:__init(rate, range)
   self.rate = rate
   self.curTime = 0
   self.difficulty = 1
   self.range = range
   self.spawnX = {
      left = 1,
      right = lWin.getWidth(),
      top = range,
      bottom = 1 
   }
   self.spawnY = {
      left = lWin.getWidth(),
      right = lWin.getWidth() + range,
      top = 1,
      bottom = lWin.getHeight() + range
   }
end

function SpawnSys:update(dt)
   self.curTime = self.curTime + dt
   if self.curTime > self.rate then
      self:spawnTop()
      self:spawnRight()
      self.curTime = self.curTime - self.rate
   end
end

function SpawnSys:spawnTop()
   local section = self.spawnX.right / self.difficulty
   for idx=self.spawnX.left, self.spawnX.right, section do
      local xPos = love.math.random(idx, idx + section)
      local yPos = -love.math.random(self.spawnX.bottom, self.spawnX.top)

      local img, animation = self:getEnemy()
      local entity = GameCreator:createBoundedImage(img, xPos, yPos,
         stack:current().world, "kinematic")

      if animation then
         entity:add(animation[1])
         entity:add(SpriteModeCmp("loop", animation))
         entity:add(QuadCmp(animation[1].anim[1]))
      end

      entity:add(VelocityCmp(gVelocity.x, gVelocity.y))
      entity:add(ForceCmp(gForce.x, gForce.y))
      entity:add(IsEnemy())
      stack:current().engine:addEntity(entity)

      lastIdx = idx
   end
end

function SpawnSys:spawnRight()
   local section = self.spawnY.bottom / self.difficulty
   for idx=self.spawnY.top, self.spawnY.bottom, section do
      local xPos = love.math.random(self.spawnY.left, self.spawnY.right)
      local yPos = love.math.random(idx, idx + section) - self.range 

      local img, animation = self:getEnemy()
      local entity = GameCreator:createBoundedImage(img, xPos, yPos,
         stack:current().world, "kinematic")

      if animation then
         entity:add(animation[1])
         entity:add(SpriteModeCmp("loop", animation))
         entity:add(QuadCmp(animation[1].anim[1]))
      end

      entity:add(VelocityCmp(gVelocity.x, gVelocity.y))
      entity:add(ForceCmp(gForce.x, gForce.y))
      entity:add(IsEnemy())
      stack:current().engine:addEntity(entity)

      lastIdx = idx
   end
end

function SpawnSys:getEnemy()
   local enemy = love.math.random(0, 2)
   local img
   local animation

   if enemy == 0 then
      img = resources.images.pollen
      animation = { SpriteFrameCmp(resources.animations.pollen_float, 0.2, 4) }
   elseif enemy == 1 then
      img = resources.images.butterfly
      animation = { SpriteFrameCmp(resources.animations.butterfly_fly, 0.3, 4) }
   else
      img = resources.images.leaf
      animation = { SpriteFrameCmp(resources.animations.leaf_float, 0.1, 9) }
   end

   return img, animation
end
