EnemyPhysSys = class("EnemyPhysSys", System)

function EnemyPhysSys:__init(range, engine)
   self.range = range
   self.engine = engine
end

function EnemyPhysSys:update(dt)
   for i, entity in pairs(self.targets) do
      local pos = entity:get("PositionCmp")
      local phys = entity:get("PhysicsCmp")
      local vel = entity:get("VelocityCmp")
      local force = entity:get("ForceCmp")

      vel.x = self:syncVelocity(vel.x, force.x, dt)
      vel.y = self:syncVelocity(vel.y, force.y, dt)
      pos.x = self:calculatePos(pos.x, vel.x, force.x, dt)
      pos.y = self:calculatePos(pos.y, vel.y, force.y, dt)
      self:syncPhys(pos, phys.body)
      self:posLimit(pos, entity)
   end
end

function EnemyPhysSys:syncVelocity(vel, force, dt)
   return vel + force * dt
end

function EnemyPhysSys:calculatePos(pos, vel, force, dt)
   return pos + vel * dt + 0.5 * force * dt * dt
end

function EnemyPhysSys:posLimit(pos, entity)
   if pos.x < -self.range or pos.y > lWin.getHeight() + self.range then
      self.engine:removeEntity(entity)
   end
end

function EnemyPhysSys:syncPhys(pos, phys)
   phys:setPosition(pos.x, pos.y)
end

function EnemyPhysSys:requires()
   return { "PositionCmp", "VelocityCmp", "ForceCmp", "PhysicsCmp" }
end
