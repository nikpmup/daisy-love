BubbleCreator = class("BubbleCreator", System)

function BubbleCreator:__init(radius, velocity, spawnRate)
   self.curTime = 0
   self.radius = radius
   self.velocity = velocity
   self.spawnRate = spawnRate
end

function BubbleCreator:update(dt)
   if lMou.isDown("l") and self.curTime > self.spawnRate then
      self:spawnBubble()
      self.curTime = 0
   end
   self.curTime = self.curTime + dt
end

function BubbleCreator:spawnBubble()
   local entity = Entity()
   local img = resources.images.smoke
   local x = lMou.getX()
   local y = lMou.getY()
   local curStack = stack:current()

   entity:add(ImageCmp(img))
   entity:add(ScaleCmp(1, 1))
   entity:add(PositionCmp(x, y))
   entity:add(VelocityCmp(self.velocity, 0))
   entity:add(ForceCmp(0, 0))

   local shape = lPhy.newCircleShape(self.radius)
   local body = lPhy.newBody(curStack.world, x, y, "kinematic")
   local fixture = lPhy.newFixture(body, shape)
   fixture:setUserData(entity)

   entity:add(PhysicsCmp(body, shape, fixture))

   curStack.engine:addEntity(entity)
end
