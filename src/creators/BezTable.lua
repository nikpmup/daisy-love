BezTable = class("BezTable")

function BezTable:__init(max, timeout)
   self.entity = Entity()
   self.curBez = 1
   self.maxBez = max
   self.timeout = timeout
   self.bezTable = self:createTable(max)
   self.timeTable = self:createTime(max)
   self.offsetTable = {}
   self.physTable = {}

   self.ptIdx = 1
   self.ptMax = 3
end

function BezTable:update(dt)
   for idx=1, self.maxBez, 1 do
      self.timeTable[idx] = self.timeTable[idx] + dt

      local table = self.offsetTable[idx]
      if table ~= nil then
         table.velX = self:syncVelocity(table.velX, table.forceX, dt)
         table.velY = self:syncVelocity(table.velY, table.forceY, dt)
         table.x = self:calculatePos(table.velX, table.forceX, dt)
         table.y = self:calculatePos(table.velY, table.forceY, dt)

         self:syncOffset(table, idx)
      end
   end
end

function BezTable:draw()
   for idx=1, self.maxBez, 1 do
      if self.timeTable[idx] < self.timeout then
         self:destroyFixture(self.physTable[idx])
         self:destroyOffset(idx)
         self:createOffset(idx)
         self.physTable[idx] = self:createPhys(self.bezTable[idx]) 

         love.graphics.line(self.bezTable[idx]:render()) 
      else
         self:destroyFixture(self.physTable[idx])
         self:destroyOffset(idx)
         self.physTable[idx] = nil
      end
   end
end

function BezTable:syncOffset(table, idx)
   self.bezTable[idx]:translate(table.x, table.y)
   local fixtures = self.physTable[idx]
   local body
   for idx=1, #table, 1 do
      body = fixtures[idx]:getBody()
      body:setX(body:getX() + table.x)
      body:setY(body:getY() + table.y)
   end
end

function BezTable:destroyFixture(table)
   if table then
      for idx=1, #table, 1 do
         table[idx]:destroy()
      end
   end
end

function BezTable:createPhys(bez)
   local table = {}
   local res = 10
   local count = 1
   local x, y
   local x2, y2
   for idx=2, res, 1 do
      x, y = bez:evaluate((idx-1)/res)
      x2, y2 = bez:evaluate(idx/res)
      local shape = lPhy.newEdgeShape(x, y, x2, y2)
      local body = lPhy.newBody(stack:current().world, 0, 0, "kinematic")
      local fixture = lPhy.newFixture(body, shape)
      fixture:setUserData(self.entity)

      table[count] = fixture
      count = count + 1
   end

   return table 
end

function BezTable:createOffset(idx)
   self.offsetTable[idx] = {
      x = 0,
      y = 0,
      velX = gVelocity.x,
      velY = 0,
      forceX = 0,
      forceY = 0
   }
end

function BezTable:destroyOffset(idx)
   self.offsetTable[idx] = nil
end

function BezTable:insertPos(x, y)
   local lastBez = self.curBez - 1
   if self.curBez > self.maxBez or self.curBez == 1 then
      self.curBez = 1
      lastBez = self.maxBez
   end

   local bez = self.bezTable[self.curBez]

   if self.ptIdx == 1 then
      if self.timeTable[lastBez] <= self.timeout then
         local last = self.bezTable[lastBez]

         bez:setControlPoint(1, last:getControlPoint(self.ptMax))
         bez:setControlPoint(2, last:getControlPoint(self.ptMax))
         bez:setControlPoint(3, last:getControlPoint(self.ptMax))
      else
         bez:setControlPoint(1, x, y)
         bez:setControlPoint(2, x, y)
         bez:setControlPoint(3, x, y)
      end
   else
      bez:setControlPoint(self.ptIdx, x, y)
   end

   self.ptIdx = self.ptIdx + 1

   if self.ptIdx > self.ptMax then
      self.timeTable[self.curBez] = 0
      self.ptIdx = 1
      self.curBez = self.curBez + 1
   end
end

function BezTable:createTable(max)
   local bezTable = {}

   for idx=1, max, 1 do
      bezTable[idx] = love.math.newBezierCurve(0, 0, 0, 0, 0, 0)
   end

   return bezTable
end

function BezTable:createTime(max)
   local time = {}

   for idx=1, max, 1 do
      time[idx] = self.timeout
   end

   return time 
end

function BezTable:syncVelocity(vel, force, dt)
   return vel + force * dt
end

function BezTable:calculatePos(vel, force, dt)
   return vel * dt + 0.5 * force * dt * dt
end
