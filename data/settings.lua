Settings = class("Settings")

function Settings:__init()
   self.curRes = 4
   self.resolutions = {{1024, 576}, {1280, 720}, {1366, 768}, {1440, 900}, {1600, 900}, {1920, 1080}}
   self.settings = {
      resolution = {1440, 900},
      fullscreen = true,
      audio = 100,
      music = 100,
      mousespeed = 1,
   }
end
