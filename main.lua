-- Importing Libraries
require("libs/Lovetoys/src/engine")
require("libs/ShortLove/ShortLove")
flux = require "libs/flux/flux"

-- Imporitng Core
require("src/core/resources")
require("src/core/stackhelper")
require("src/core/state")

-- States
require("src/states/MenuState")

-- Utils
require("src/utils/saves")
require("src/utils/tables")
require("src/utils/SpriteExtractor")

-- Data
require("data/settings")

function love.load()
   resources = Resources()

   -- Load Image 
   resources:addImage("grass", "res/img/grass.png")
   resources:addImage("plant_big", "res/img/plant_big.png")
   resources:addImage("plant_small", "res/img/plant_small.png")
   resources:addImage("butterfly", "res/img/butterfly.png")
   resources:addImage("dandelion", "res/img/dandelion.png")
   resources:addImage("pollen", "res/img/pollen.png")
   resources:addImage("Menemy", "res/img/m-enemy.png")
   resources:addImage("smoke", "res/img/smoke.png")
   resources:addImage("leaf", "res/img/leaf.png")
   resources:addImage("background", "res/img/Background.png")

   -- Load Sound

   -- Load Fonts
   resources:addFont("arial_48", "res/fonts/arial.ttf", 48)
   resources:addFont("arial_32", "res/fonts/arial.ttf", 32)

   resources:load()

   -- Load Animations
   resources:saveAnimation("pollen_float", SpriteExtractor:pullAnimation(
         resources.images.pollen, 16, 16, 1, 4))
   resources:saveAnimation("dendelion_float", SpriteExtractor:pullAnimation(
         resources.images.dandelion, 32, 32, 1, 4))
   resources:saveAnimation("butterfly_fly", SpriteExtractor:pullAnimation(
         resources.images.butterfly, 32, 32, 1, 4))
   resources:saveAnimation("leaf_float", SpriteExtractor:pullAnimation(
         resources.images.leaf, 64, 64, 1, 9))

   -- Load Settings
   set = Settings()
   Saves():loadSettings()
   Saves():refreshSettings()

   -- Loading Stack
   stack = StackHelper()
   stack:push(MenuState())

   gVelocity = {
      x = -100,
      y = 100
   }

   gForce = {
      x = -40,
      y = 20
   }
end

function love.update(dt)
   stack:update(dt)
end

function love.draw()
   stack:draw()
end

function love.keypressed(key, isRepeat)
   stack:current():keypressed(key, isRepeat)
end

function love.mousepressed(x, y, button)
   stack:current():mousepressed(x, y, button)
end

function love.mousereleased(x, y, button)
   stack:current():mousereleased(x, y, button)
end

function love.quit()
   Saves:saveSettings()
   -- Add save stuff
end
